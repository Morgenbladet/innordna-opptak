import React, { Component } from 'react';

class Programme extends Component {
  constructor(props) {
    super(props);
    this.goToURL = this.goToURL.bind(this);
  }
  
  goToURL() {
    window.open(this.props.data.url, '_blank');
  }
  
  render() {
    let d = this.props.data;
    const flags = d.flags.map((flag, i) => <span key={i} className="flag">{ flag }</span>);
    return(
      <div className="Programme" onClick={ this.goToURL }>
	<div className="programme-title">
	  { d.name }
	</div>
	<div className="programme-info">
	  <span className="level">{ d.level }, { d.length } halvår.</span>&nbsp;
	  <abbr className="institution" title={ d.institution.name }>
	    { d.institution.shortName }
	  </abbr>.&nbsp;
	  Studiested: <span className="location">{ d.place }</span>&nbsp;
	  <span className="flags">{ flags }</span>
	</div>
      </div>
    );
  }
}

export default Programme;

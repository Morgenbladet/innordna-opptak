import React, { Component } from 'react';
import Programme from './Programme.js';

class Leaf extends Component {
  constructor(props) {
    super(props);
    this.ref = React.createRef();
  }
  componentDidMount() {
    this.ref.current.scrollIntoView({ behavior: 'smooth', block: 'start' } );
    window.scrollBy(0,-20);
  }

  render() {
    const programmes = this.props.programmes.map((prg) => 
						 <li key={prg.id}><Programme data={ prg } /></li>
    );
    return(
      <div className="Leaf" ref={ this.ref }>
        <p>OK, nå tror jeg at jeg har funnet noen passende alternativer for deg…</p>
        <ul className="programmes">{ programmes }</ul>
      </div>
    );
  }
}

export default Leaf;

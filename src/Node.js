import React, { Component } from 'react';

class Node extends Component {
  constructor(props) {
    super(props);
    this.ref = React.createRef();
    
    this.state = { 
      selected_alternative: null,
      next_node: null
    };
  }

  componentDidMount() {
    if (this.props.scroll) {
      this.ref.current.scrollIntoView({ behavior: 'smooth', block: 'center' } );
    }
  }
  
  selectAlternative(id, e) {
    this.setState((prevState, props) => ({
      selected_alternative: id,
      next_node: this.props.generator(id, props.num + 1 )
    }));
    
    e.preventDefault();
  }

  render() {
    const alternatives = this.props.data.alternatives.map(
      (alt) => <li key={ alt.jump }>
	<button disabled={ this.state.selected_alternative === alt.jump } onClick={ (e) => this.selectAlternative(alt.jump, e) }  dangerouslySetInnerHTML={ { __html: alt.text } }></button>
	</li>
    );
    
    return(
      <div className="Node" ref={ this.ref }>
	<p><span className="num">{ this.props.num }. </span>
	  <span dangerouslySetInnerHTML={ {__html: this.props.data.text} }></span>
	</p>
	
        <ul className="alternatives">
          { alternatives }
        </ul>
        { this.state.next_node }
      </div>
    );
  }
}

export default Node;

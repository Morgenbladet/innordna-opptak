import React, { Component } from 'react';
import './App.css';
import Node from './Node.js';
import Leaf from './Leaf.js';
import nodes from './data/nodes.json';
import programmes from './data/programmes.json';

class App extends Component {
  constructor(props) {
    super(props);


    this.renderNode = this.renderNode.bind(this);

    this.state = {
      node_data: nodes,
    };
  }
  
  renderNode(id, num=1, scroll=true) {
    if (id.startsWith("L:")) {
      // We reached a leaf node
      let leaf_name = id.slice(2);
      let prgs = programmes.filter((prg) => prg.belongs_to.includes(leaf_name));
      return (<Leaf key={ leaf_name } id={ leaf_name } programmes={ prgs }/>);
    } else {
      let node = this.state.node_data.find(node => node.id === id);
      if (!node) {
        return (<p>Node not found: { id }.</p>);
      } else {
        return (
          <Node
            key={node.id + "_" + num}
  	    scroll={scroll}
	    num={num}
            data={node}
            generator={this.renderNode}
          />
        );
      }
    }
  }

  render() {
    return (
      <div className="App">
	{ this.renderNode("ROOT", 1, false) }
      </div>
    );
  }
}

export default App;

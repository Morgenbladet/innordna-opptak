require 'roo'
require 'json'

OUTPUTDIR = File.join(__dir__, "..", "src", "data")
DATAFILE = ARGV[0] || File.join(__dir__, "datasett.ods")
NODES_JSON = ARGV[1] || File.join(OUTPUTDIR, "nodes.json")
PROGRAMMES_JSON = ARGV[2] || File.join(OUTPUTDIR, "programmes.json")
GRAPH_FILE = File.join(__dir__, "graph.gv")
NODE_DB = File.join(__dir__, 'data.json')

spreadsheet = Roo::Spreadsheet.open(DATAFILE)

nodes = spreadsheet.
  sheet('noder').
  parse(id: /NAME/, text: /TEXT/, alt1: /ALT1/, alt2: /ALT2/, jump1: /JUMP1/, jump2: /JUMP2/).
  select{|n| !(n[:id].nil? || n[:id] == "") }.
  map do |node|
    {
      id: node[:id],
      text: node[:text],
      alternatives: [
        {
          text: node[:alt1],
          jump: node[:jump1]
        },
        {
          text: node[:alt2],
          jump: node[:jump2]
        }
      ]
    }
  end

File.open(NODES_JSON, "w") do |f|
  f << JSON.pretty_generate(nodes)
end
puts "#{nodes.size} nodes written to #{NODES_JSON}"

# We need some extra data from the nodes database, as well
nodedb = JSON.parse(File.read(NODE_DB))["data"]
ids = nodedb.map{|n| n["id"] }
unless ids.size == ids.uniq.size
  puts "[W] Not all nodes in NodeDB are unique!"
end


programmes = spreadsheet.
               sheet('studier').
               parse(id: /ID/, name: /NAME/, place: /PLACE/, institution: /INSTITUTION/,
                     level: /LEVEL/, category: /CATEGORY/,
                     leaf1: /LEAF1/, leaf2: /LEAF2/, leaf3: /LEAF3/, leaf4: /LEAF4/).
               select{|p| !(p[:id].nil? || p[:id] == "")}

programmes.map! do |prg|
  leaves = [ prg[:leaf1], prg[:leaf2], prg[:leaf3], prg[:leaf4] ].
             select{|l| !(l.nil? || l == "") }

  node = nodedb.find{|n| n["id"] == prg[:id] }
  
  {
    id: prg[:id],
    name: prg[:name],
    url: node["url"],
    institution: {
      name: node["institution"]["name"],
      shortName: node["institution"]["shortName"]
    },
    place: prg[:place],
    level: prg[:level],
    length: node["numSemesters"],
    places: node["numStudyPlaces"],
    flags: node["flags"],
    category: prg[:category],
    belongs_to: leaves
  }
end

File.open(PROGRAMMES_JSON, "w") do |f|
  f << JSON.pretty_generate(programmes)
end

puts "#{programmes.size} nodes written to #{PROGRAMMES_JSON}"
puts "[W] #{programmes.select{|p| p[:belongs_to].size == 0}.size} belong to no leaf!"

# Check for sanity
nodekeys = nodes.map{|n| n[:id]}
nodes.each do |node|
  node[:alternatives].each do |alt|
    jmp = alt[:jump]
    if jmp.nil? || jmp == ""
      puts "[E] A jump is missing in #{node[:id]}"
    elsif jmp.slice(0,2) == "L:"
      unless programmes.detect{|p| p[:belongs_to].include?(jmp.sub("L:", ""))}
        puts "[W] Leaf #{jmp} has no programmes"
      end
    else
      unless nodekeys.include?(jmp)
        puts "[E] Jump #{jmp} jumps to non-existent node"
      end
    end
  end
  unless nodes.detect{|n| n[:alternatives].detect{|a| a[:jump] == node[:id]}}
    puts "[W] No path leads to node #{node[:id]}"
  end
end

File.open(GRAPH_FILE, "w") do |f|
  nodekeys = nodes.map{|n| n[:id]}
  leaves = nodes.map{|n| n[:alternatives].map{|a| a[:jump]}}.flatten.uniq.select{|k| !k.nil? && k.slice(0,2) == "L:"}
  f.puts "digraph nodes {"
  nodekeys.each do |key|
    f.puts "\"#{key}\" [shape=diamond];"
  end
  leaves.each do |key|
    f.puts "\"#{key}\" [shape=plaintext];"
  end
  nodes.each do |node|
    alts = node[:alternatives].map{|a| "\"#{a[:jump]}\""}.join(" ")
    f.puts "\t\"%s\" -> { %s };" % [ node[:id], alts ]
  end
  f.puts "}"
end

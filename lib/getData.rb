require 'net/http'
require 'json'
require 'certified'

uri = URI('https://sok.samordnaopptak.no/api/v1/search')

req = Net::HTTP::Post.new(uri)
req.content_type = 'application/json'

req.body = {
  from: 0,
  size: 1500,
  filter: [
    { term: { "admission.id": 4 } }
  ],
  sort: [
    { "names.NB.keyword": { order: 'asc' } }
  ]
}.to_json

Net::HTTP.start(uri.host, uri.port, use_ssl: true) do |http|
  @res = http.request(req)
end

File.open("data.json", "w") do |f|
  # A little silly, but this sanity checks and pretty-prints
  f << JSON.pretty_generate(JSON.parse(@res.body))
end

require 'json'
require 'csv'
require 'byebug'

OUTFILE = "data.csv"

raw_data = File.read("data.json")

data = JSON.parse(raw_data)["data"]
byebug

CSV.open(OUTFILE, "wb") do |csv|
  data.each do |r|
    csv << [
      r["id"],
      r["names"]["NB"],
      r["teachingLocation"]["name"],
      r["institution"]["shortName"],
      r["studyProgrammeType"]["name"],
      r["mainEducationArea"]["name"]
    ]
  end
end
